package common.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import common.Execution;

public class PosibaDB {

	private static String driverName = "com.mysql.jdbc.Driver";

	/**
	 * 
	 * @return
	 */
	public static Connection connectOverSSH() {
		String sshUser = "ec2-user";
		String sshHost = "54.68.21.0";
		int sshPort = 22;
		int localPort = 1234;
		int dbPort = 3306;
		String dbUser = "posiba";
		String dbPass = "@melinh#02";
		String dbHost = "127.0.0.1";
		String dbName = "posiba_core";
		// String sshKey = "src\\main\\java\\files\\posiba-dev.pem";
		String sshKey = Execution.FILES_PATH + "posiba-dev.pem";
		Connection conn = null;
		try {
			// Remote to SSH host
			JSch jsch = new JSch();
			Session session = jsch.getSession(sshUser, sshHost, sshPort);
			jsch.addIdentity(sshKey);
			JSch.setConfig("StrictHostKeyChecking", "no");
			System.out.println("[SSH] Establishing Connection...");
			session.connect();
			// session.delPortForwardingL(localPort);
			// Forward port
			int assingedPort = session.setPortForwardingL(localPort, dbHost,
					dbPort);
			System.out.println("localhost:" + assingedPort + " -> " + dbHost
					+ ":" + dbPort);
			System.out.println("[SSH] Port Forwarded");

			String url = "jdbc:mysql://" + dbHost + ":" + localPort + "/";

			// Connect MySQL database
			try {
				Class.forName(driverName);
				conn = DriverManager
						.getConnection(url + dbName, dbUser, dbPass);
				System.out.println("[MySQL] Database connection established.");
				conn.setAutoCommit(false);
			} catch (Exception ex) {
				System.out.println("[MySQL] Database connection failed.");
				ex.printStackTrace();
			}
			session.delPortForwardingL(localPort);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static Connection connectDB() {
		int dbPort = 3306;
//		String dbUser = "posiba";
//		String dbPass = "@melinh#02";
//		String dbHost = "givn-qa-001.int.posiba.net";
//		String dbName = "posiba_givn_staging";
		
		String dbUser = "auto_user";
		String dbPass = "W7G4eHLnkg";
		String dbHost = "givn-staging-db.int.posiba.net";
		String dbName = "posiba_givn_staging";
		
		Connection conn = null;
		String url = "jdbc:mysql://" + dbHost + ":" + dbPort + "/";
		// Connect MySQL database
		try {
			Class.forName(driverName);
			conn = DriverManager.getConnection(url + dbName, dbUser, dbPass);
			System.out.println("[MySQL] Database connection established.");
			conn.setAutoCommit(false);
		} catch (Exception ex) {
			System.out.println("[MySQL] Database connection failed.");
			ex.printStackTrace();
		}

		return conn;

	}

	public static Connection connectDB(String dbUser, String dbPass,
			String dbHost, String dbName) {
		int dbPort = 3306;
		Connection conn = null;
		String url = "jdbc:mysql://" + dbHost + ":" + dbPort + "/";
		// Connect MySQL database
		try {
			Class.forName(driverName);
			conn = DriverManager.getConnection(url + dbName, dbUser, dbPass);
			System.out.println("[MySQL] Database connection established.");
			conn.setAutoCommit(false);
		} catch (Exception ex) {
			System.out.println("[MySQL] Database connection failed.");
			ex.printStackTrace();
		}

		return conn;

	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	public static ResultSet executeQuery(String query) {
		Statement stmt = null;
		ResultSet rs = null;
		Connection conn = connectDB();
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			conn.commit();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return rs;
	}

	/**
	 * 
	 * @param conn
	 * @param query
	 * @return
	 */
	public static ResultSet executeQuery(Connection conn, String query) {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			conn.commit();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
		return rs;
	}

	/**
	 * 
	 * @param query
	 */
	public static void executeUpdate(String query) {
		Statement stmt = null;
		Connection conn = connectDB();
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			conn.commit();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public static void executeUpdate(Connection conn, String query) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			conn.commit();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	/**
	 * Get data from DB. Author: Khoi Date: Jan 17, 2015
	 * 
	 * @param query
	 * @param column
	 * @return
	 */
	public static String getData(String query, String column) {
		String value = "";
		ResultSet rs = executeQuery(query);
		try {
			while (rs.next()) {
				value = rs.getString(column);
			}
		} catch (SQLException e) {
			System.out.println("Cannot get data from DB!");
		}
		return value;
	}

	/**
	 * Get data from DB. Author: Khoi Date: Jan 17, 2015
	 * 
	 * @param query
	 * @param column
	 * @return
	 */
	public static String getData(Connection conn, String query, String column) {
		String value = "";
		ResultSet rs = executeQuery(conn, query);
		try {
			while (rs.next()) {
				value = rs.getString(column);
			}
		} catch (SQLException e) {
			System.out.println("Cannot get data from DB!");
		}
		return value;
	}

	public static List<String> getDataList(Connection conn, String query,
			String column) {
		List<String> values = new ArrayList<String>();

		ResultSet rs = executeQuery(conn, query);
		try {
			while (rs.next()) {
				values.add(rs.getString(column));
			}
		} catch (SQLException e) {
			System.out.println("Cannot get data from DB!");
		}
		return values;
	}

	public static List<String> getDataList(String query, String column) {
		List<String> values = new ArrayList<String>();

		ResultSet rs = executeQuery(query);
		try {
			while (rs.next()) {
				values.add(rs.getString(column));
			}
		} catch (SQLException e) {
			System.out.println("Cannot get data from DB!");
		}
		return values;
	}

	public static List<String> getDataList(String query, String columnName,
			String[] parameters) {

		Connection conn = connectDB();
		PreparedStatement stmt = null;
		ResultSet rs;
		List<String> values = new ArrayList<String>();
		try {
			stmt = conn.prepareStatement(query);
			int j = 1; // Parameter index starts at 1
			for (int i = 0; i <= parameters.length - 1; i++) {
				stmt.setString(j++, parameters[i]);
			}
			rs = stmt.executeQuery();
			conn.commit();
			while (rs.next()) {
				values.add(rs.getString(columnName));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return values;
	}

	/**
	 * 
	 * Author: Khoi Date: Jan 17, 2015
	 * 
	 * @param query
	 * @param columns
	 * @return
	 */
	public static Hashtable<String, String> getData(String query,
			String[] columns) {
		Hashtable<String, String> value = new Hashtable<String, String>();
		ResultSet rs = executeQuery(query);
		try {
			while (rs.next()) {
				for (String column : columns) {
					value.put(column, rs.getString(column));
				}
			}
		} catch (SQLException e) {
			System.out.println("Cannot get data from DB!");
		}
		return value;
	}

	/**
	 * Author: Khoi Date: Dec 09, 2015
	 * 
	 * @param conn
	 * @param query
	 * @param columns
	 * @return
	 */
	public static Hashtable<String, String> getData(Connection conn,
			String query, String[] columns) {
		Hashtable<String, String> value = new Hashtable<String, String>();
		ResultSet rs = executeQuery(conn, query);
		try {
			while (rs.next()) {
				for (String column : columns) {
					value.put(column, rs.getString(column));
				}
			}
		} catch (SQLException e) {
			System.out.println("Cannot get data from DB!");
		}
		return value;
	}

	public static void executeUpdatedSP(String storeProcedure, String value) {
		CallableStatement stmt = null;
		Connection conn = connectDB();
		try {
			String query = "call " + storeProcedure + "(?)";
			stmt = conn.prepareCall(query);
			stmt.setString(1, value);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	public static void executeUpdatedSP(String storeProcedure, String value1,
			String value2) {
		CallableStatement stmt = null;
		Connection conn = connectDB();
		try {
			String query = "call " + storeProcedure + "(?,?)";
			stmt = conn.prepareCall(query);
			stmt.setString(1, value1);
			stmt.setString(2, value2);
			stmt.executeUpdate();
			conn.commit();
		} catch (SQLException ex) {
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		}
	}

	/**
	 * 
	 * @param conn
	 */
	public static void close(Connection conn) {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}