package common.thirdParty;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;

import org.jsoup.Jsoup;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import web.DriverWeb;
import web.function.ActWebGen;
import web.function.VerWebGen;
import common.Execution;

public class Mail {

	public WebDriver _driver;
	public String enabledtls = "true";
	public int timeout; // in minutes
	public String server;
	public String username;
	public String password;
	public String folder;
	public String subject;
	public String from;
	public String to;
	public String content;
	public String date; // Format: Mmm dd (Ex: Oct 17)
	public int number;
	public String protocol;
	public String host;
	public int port;

	public Mail(WebDriver driver) {
		_driver = driver;
		server = "imap.gmail.com";
		folder = "inbox";
		protocol = "imaps";
		host = "imap.gmail.com";
		port = 465;
		timeout = 5;
	}

	public Mail(WebDriver driver, String _username, String _password) {
		_driver = driver;
		server = "imap.gmail.com";
		folder = "inbox";
		protocol = "imaps";
		host = "imap.gmail.com";
		port = 465;
		timeout = 5;
		username = _username;
		password = _password;
	}

	public Store connectMailServer() {
		Store store = null;
		int i = 0;
		while (i < 10) {
			try {
				Properties props = new Properties();
				props.put("mail.store.protocol", protocol);
				props.put("mail." + protocol + ".host", host);
				props.put("mail." + protocol + ".port", port);
				props.put("mail." + protocol + ".starttls.enable", enabledtls);

				Session emailSession = Session.getDefaultInstance(props);
				store = emailSession.getStore(protocol);
				System.out.println("Connect to " + server + " with credentials: " + username + "/"
				    + password);
				i++;
				store.connect(server, username, password);
				ActWebGen.sleep(2000);
				if (store.isConnected()) {
					return store;
				}
				store.close();
				System.out.println("Cannot connect to mail server, retry " + i);
			} catch (MessagingException e) {
				// e.printStackTrace();
			}
		}
		return store;
	}

	public void disconnectMailServer(Store store) {
		try {
			store.close();
			System.out.println("Disconnected to the Mail Server");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public Folder connectMailFolder(Store store) {
		Folder mailFolder = null;
		try {
			mailFolder = store.getFolder(folder);
			mailFolder.open(Folder.READ_WRITE);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return mailFolder;
	}
	
	public Folder connectMailFolder() {
		Store store = connectMailServer();
		Folder mailFolder = null;
		try {
			mailFolder = store.getFolder(folder);
			mailFolder.open(Folder.READ_WRITE);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return mailFolder;
	}

	public void disconnectMailFolder(Folder mailFolder) {
		try {
			mailFolder.expunge();
			mailFolder.close(true);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public int getMailNumber(Folder mailFolder) {
		String existed = "false";
		int n = timeout;
		number = -1;
		if (subject != null) {
			try {
				while (n > 0 && existed.equals("false")) {
					Message[] messages = mailFolder.getMessages();
					int totalMess = messages.length;

					for (int i = totalMess - 1; i > -1; i--) {
						String mailSubject = messages[i].getSubject();
						existed = "true";
						if (!mailSubject.equals(subject)) {
							existed = "false";
						}
						if (from != null) {
							String mailFrom = messages[i].getFrom()[0].toString();

							if (!mailFrom.contains(from)) {
								existed = "false";
							}
						}
						if (date != null) {
							String mailDate = messages[i].getReceivedDate().toString();

							if (!mailDate.contains(date)) {
								existed = "false";
							}
						}

						if (content != null) {
							Object mailContent = messages[i].getContent();
							String mailBody = "";

							if (mailContent instanceof String) {
								mailBody = messages[i].getContent().toString();
							} else if (mailContent instanceof Multipart) {
								Multipart multipart = (Multipart) messages[i].getContent();
								for (int j = 0; j < multipart.getCount(); j++) {
									BodyPart bodyPart = multipart.getBodyPart(j);
									String disposition = bodyPart.getDisposition();
									if (disposition == null) {
										mailBody = mailBody + bodyPart.getContent().toString();
									}
								}
							}
							mailBody = Jsoup.parse(mailBody).toString();

							if (!mailBody.contains(content)) {
								existed = "false";
							}
						}

						if (existed.equals("true")) {
							number = i;
							break;
						}
					}

					if (existed.equals("false")) {
						System.out.println("Message not found. Wait 1 minute and will check again..." + n);
						number = -1;
						ActWebGen.sleep(60000);
						n = n - 1;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Execution.setTestFail("Subject is required.");
		}
		return number;
	}


	public String getMailContent(Folder mailFolder) {
		String body = "";
		number = getMailNumber(mailFolder);
		if (number != -1) {
			try {
				Message[] messages = mailFolder.getMessages();
				Object mailContent = messages[number].getContent();
				if (mailContent instanceof String) {
					body = messages[number].getContent().toString();
				} else if (mailContent instanceof Multipart) {
					Multipart multipart = (Multipart) messages[number].getContent();
					for (int j = 0; j < multipart.getCount(); j++) {
						BodyPart bodyPart = multipart.getBodyPart(j);
						String disposition = bodyPart.getDisposition();
						if (disposition == null) {
							body = body + bodyPart.getContent().toString();
						}
					}
					body = Jsoup.parse(body).toString();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return body;
	}

	public String getTextLinkInEmail(Folder mailFolder, String linktext) {
		String body = getMailContent(mailFolder);
		int stringpos = body.indexOf(linktext + "</a>");
		String mainstring = body.substring(0, stringpos);
		int hrefpos = mainstring.lastIndexOf("href=" + '"');
		String hrefstring = mainstring.substring(hrefpos + 6, mainstring.length());
		int endpos = hrefstring.indexOf('"');
		String url = hrefstring.substring(0, endpos);
		return url;
	}

	public void accessLinkInEmail(Folder mailFolder, String linktext) {
		String url = getTextLinkInEmail(mailFolder, linktext);
		DriverWeb.instance.findElement(By.tagName("html")).sendKeys(Keys.CONTROL + "t");
		DriverWeb.instance.get(url);
	}

	public void checkMailContent(Folder mailFolder, String text) {
		System.out.println("Checking mail content...");
		String body = getMailContent(mailFolder);
		if (body != "") {
			VerWebGen.verifyTextContains(body,text);
		}
	}

	public void deleteMail(Folder mailFolder) {
		System.out.println("Deleting mail...");
		try {
			Message[] messages = mailFolder.getMessages();
			int totalMess = messages.length;
			for (int i = totalMess - 1; i > -1; i--) {
				if (messages[i].getSubject().equals(subject)) {
					messages[i].setFlag(Flags.Flag.DELETED, true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteAllMail(Folder mailFolder) {
		System.out.println("Deleting all mail...");
		try {
			Message[] messages = mailFolder.getMessages();
			/*
			 * int totalMess = messages.length; for (int i = totalMess-1; i > -1; i--) {
			 * messages[i].setFlag(Flags.Flag.DELETED, true); mailFolder.expunge(); }
			 */
			Flags deleted = new Flags(Flags.Flag.DELETED);
			mailFolder.setFlags(messages, deleted, true);
			mailFolder.expunge();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void verifyMailExists(Folder mailFolder) {
		System.out.println("Checking mail " + subject + " exist...");
		number = getMailNumber(mailFolder);
		if (number == -1) {
			Execution.setTestFail("The mail " + subject + " does not exists.");
		}
	}

	public void verifyMailNotExists(Folder mailFolder) {
		System.out.println("Checking mail " + subject + " not exist...");
		number = getMailNumber(mailFolder);
		if (number != -1) {
			Execution.setTestFail("The mail " + subject + " exists.");
		}
	}
	
}