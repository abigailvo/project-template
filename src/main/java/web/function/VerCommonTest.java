package web.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Folder;
import javax.mail.Store;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import web.DriverWeb;
import web.page.PgCommon;
import common.Execution;
import common.thirdParty.Mail;

public class VerCommonTest {

	// -------------------------------------------Automation Common Checklist---------------------------------
	/**
	 * CTC02	Boundary testing for text box length with valid values - CASE: control DON'T has maxlength defined with specific value in code
	 * CTC03	Boundary testing for text box length with invalid values - CASE: control DON'T has maxlength defined with specific value in code 
	 */
	public static void CTC02_CTC03_verifyTextboxBoundary(By by, WebElement submitButton, int min, int max, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC02_CTC03_verifyTextboxBoundary(element, submitButton, min, max,  errorMessage, isValid);
	}

	
	public static void CTC02_CTC03_verifyTextboxBoundary(WebElement textbox, WebElement submitButton, int min, int max,
			 String errorMessage, boolean isValid) {
		  // WebElement errorElement, String errorMessage, boolean isValid) {
		
		if (textbox != null) {
			String[] arrData;
			if (isValid) {
				//valid case
				if (min ==1){
					// is mandatory field
					String data1 = ActWebGen.randomString(min);
					String data2 = ActWebGen.randomString(min + 1);
					String data3 = ActWebGen.randomString((min + max) / 2);
					String data4 = ActWebGen.randomString(max - 1);
					String data5 = ActWebGen.randomString(max);
					//String data6 = "";
					//String data7 = " ";
					//String data8 = "          ";
					arrData = new String[] { data1, data2, data3, data4, data5 };
					for (String data : arrData) {
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						//WebElement elementError2 = _elem.;
						ActWebGen.sleep(1000);
						//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
						VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}
				}
				else
				{
					// is NOT mandatory field
					String data1 = ActWebGen.randomString(min);
					String data2 = ActWebGen.randomString(min + 1);
					String data3 = ActWebGen.randomString((min + max) / 2);
					String data4 = ActWebGen.randomString(max - 1);
					String data5 = ActWebGen.randomString(max);
					String data6 = "";
					String data7 = " ";
					String data8 = "          ";
					arrData = new String[] { data1, data2, data3, data4,
							data5, data6, data7, data8 };
					for (String data : arrData) {
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
						//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
						VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}
				}
				
			} else {
				String[] arrInvalidData;	
				if(min==1)
				{
					// is mandatory field
					String dataInvalid1 = ActWebGen.randomString(min-1);
					//String dataInvalid2 = ActWebGen.randomString(max + 1);
					//String dataInvalid3 = ActWebGen.randomString(9999);
					String dataInvalid4 = ActWebGen.randomString(0);
					String dataInvalid5 = "";
					String dataInvalid6 = " ";
					String dataInvalid7 = "          ";
					arrInvalidData = new String[] { dataInvalid1,
							dataInvalid4, dataInvalid5, dataInvalid6, dataInvalid7 };
					for (String data : arrInvalidData) 
					{
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						ActWebGen.sleep(1000);
						//VerWebGen.verifyTextContains(errorElement, errorMessage);
						//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
						VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}								
				}
				else{
					// is NOT mandatory field
					String dataInvalid1 = ActWebGen.randomString(min-1);
					String dataInvalid2 = ActWebGen.randomString(max + 1);
					String dataInvalid3 = ActWebGen.randomString(99999);
					String dataInvalid4 = ActWebGen.randomString(0);
					arrInvalidData = new String[] { dataInvalid1,
							dataInvalid2, dataInvalid3, dataInvalid4};
					for (String data : arrInvalidData) 
					{
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						//VerWebGen.verifyTextContains(errorElement, errorMessage);
						//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
						VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}
				}									
			}
		} 
		else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ textbox + " does not exists.");
	
	
	}
	/**
	 * CTC04	Boundary testing for text box length with valid values - CASE: control has maxlength defined with specific value in code
	 * CTC05	Boundary testing for text box length with invalid values - CASE: control has maxlength defined with specific value in code 
	 */
	public static void CTC04_CTC05_verifyTextboxBoundary(By by, WebElement submitButton, int min, int max,
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC04_CTC05_verifyTextboxBoundary(element, submitButton, min, max, errorMessage, isValid);
	}

	public static void CTC04_CTC05_verifyTextboxBoundary(WebElement textbox, WebElement submitButton, int min, int max, 
			String errorMessage, boolean isValid) {
		int maxlength = Integer.parseInt(textbox.getAttribute("maxlength"));
		if (textbox != null & maxlength==max ) {
			String[] arrData;
			if (isValid) {
				//valid case
				if (min ==1){
					// is mandatory field
					String data1 = ActWebGen.randomString(min);
					String data2 = ActWebGen.randomString(min + 1);
					String data3 = ActWebGen.randomString((min + max) / 2);
					String data4 = ActWebGen.randomString(max - 1);
					String data5 = ActWebGen.randomString(max);
					//String data6 = "";
					//String data7 = " ";
					//String data8 = "          ";
					arrData = new String[] { data1, data2, data3, data4,
							data5 };
					for (String data : arrData) {
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						ActWebGen.sleep(1000);
						//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
						//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
						VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}
				}
				else
				{
					// is NOT mandatory field
					String data1 = ActWebGen.randomString(min);
					String data2 = ActWebGen.randomString(min + 1);
					String data3 = ActWebGen.randomString((min + max) / 2);
					String data4 = ActWebGen.randomString(max - 1);
					String data5 = ActWebGen.randomString(max);
					String data6 = "";
					String data7 = " ";
					String data8 = "          ";
					arrData = new String[] { data1, data2, data3, data4,
							data5, data6, data7, data8 };
					for (String data : arrData) {
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						ActWebGen.sleep(1000);
						//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
						//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
						VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}
				}
				
			} else {
				//invalid case
				String[] arrInvalidData;	
				if(min==1)
				{
					// is mandatory field
					String dataInvalid1 = "";
					String dataInvalid2 = " ";
					String dataInvalid3 = "          ";
					arrInvalidData = new String[] { dataInvalid1,
							dataInvalid2, dataInvalid3 };
					for (String data : arrInvalidData) 
					{
						textbox.clear();
						textbox.sendKeys(data);
						//textbox.sendKeys(Keys.TAB);
						submitButton.click();
						ActWebGen.sleep(1000);
						//VerWebGen.verifyTextContains(errorElement, errorMessage);
						//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
						VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
					}								
				}					
			}
		} 
		else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ textbox + " does not exists. OR the control isn't set Maxlength");
	}
	/**
	 * CTC06	Validate text box just allows alphabet characters only with valid values.
	 * CTC07	Validate text box just allows alphabet characters only with invalid values.
	 */
	public static void CTC06_CTC07_verifyOnlyAlphabetTextbox(By by, WebElement submitButton, int len, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC06_CTC07_verifyOnlyAlphabetTextbox(element, submitButton, len,  errorMessage, isValid);
	}

	public static void CTC06_CTC07_verifyOnlyAlphabetTextbox(WebElement element, WebElement submitButton, int len,
			String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		
		if (element != null) {
			if (isValid) {
				// valid case
				String data1 = ActWebGen.randomAlpha(len);
				String data2 = " " + ActWebGen.randomAlpha(len);
				String data3 = ActWebGen.randomAlpha(len) + " ";
				String data4 = ActWebGen.randomAlpha(len / 2) + " "
						+ ActWebGen.randomAlpha(len / 2);
				String[] arrData = new String[] { data1, data2, data3, data4 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			} else {
				//invalid case
				String dataInvalid1 = ActWebGen.randomSpecial(len);
				String dataInvalid2 = ActWebGen.randomNumber(len);
				String dataInvalid3 = "1 = 1";
				String dataInvalid4 = "<script>alert(document.cookie);</script>";
				String[] arrInvalidData = new String[] { dataInvalid1,
						dataInvalid2, dataInvalid3, dataInvalid4 };
				for (String data : arrInvalidData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextContains(errorElement, errorMessage);
					//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
					VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}

	/**
	 * CTC08	Validate text box just allows number only with valid values, 
	 * CTC09	Validate text box just allows number only with invalid values.
	 */
	public static void CTC08_CTC09_verifyOnlyNumberTextbox(By by, WebElement submitButton, int len, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC08_CTC09_verifyOnlyNumberTextbox(element, submitButton, len, errorMessage, isValid);
	}

	public static void CTC08_CTC09_verifyOnlyNumberTextbox(WebElement element, WebElement submitButton, int len,
			String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		
		if (element != null) {
			if (isValid) {
				//valid case
				String data1 = ActWebGen.randomNumber(len);
				String data2 = "-" + ActWebGen.randomNumber(len);
				String data3 = " " + ActWebGen.randomNumber(len);
				String data4 = ActWebGen.randomNumber(len) + " ";
				String data5 = ActWebGen.randomNumber(len / 2) + " "
						+ ActWebGen.randomNumber(len / 2);
				String[] arrData = new String[] { data1, data2, data3, data4,
						data5 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			} else {
				//invalid case	
				String dataInvalid1 = ActWebGen.randomSpecial(len);
				String dataInvalid2 = ActWebGen.randomAlpha(len);
				String dataInvalid3 = "1 = 1";
				String dataInvalid4 = "<script>alert(document.cookie);</script>";
				String[] arrInvalidData = new String[] { dataInvalid1,
						dataInvalid2, dataInvalid3, dataInvalid4 };
				for (String data : arrInvalidData) {
					element.clear();			
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextContains(errorElement, errorMessage);
					//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
					VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}

	/**
	 * CTC10	Validate text box allows all character types.
	 * 
	 */
	public static void CTC10_verifyAllCharactersTextbox(By by, WebElement submitButton, int len, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC10_verifyAllCharactersTextbox(element,submitButton, len, errorMessage, isValid);
	}

	public static void CTC10_verifyAllCharactersTextbox(WebElement element, WebElement submitButton, int len,
			String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		
		if (element != null) {
			if (isValid) {
				String data1 = ActWebGen.randomNumber(len);
				String data2 = " " + ActWebGen.randomNumber(len);
				String data3 = ActWebGen.randomNumber(len) + " ";
				String data4 = ActWebGen.randomNumber(len / 2) + " "
						+ ActWebGen.randomNumber(len / 2);
				String data5 = ActWebGen.randomSpecial(len);
				String data6 = ActWebGen.randomAlpha(len);
				String data7 = "1 = 1";
				String data8 = "<script>alert(document.cookie);</script>";
				String[] arrData = new String[] { data1, data2, data3, data4,
						data5, data6, data7, data8 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}

		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}

	/**
	 * CTC11	Validate Email Address text box with valid values 
	 * CTC12	Validate Email Address text box with invalid values
	 */
	public static void CTC11_CTC112_verifyEmailTextbox(By by, WebElement submitButton, int len, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC11_CTC112_verifyEmailTextbox(element,submitButton, len, errorMessage, isValid);
	}

	public static void CTC11_CTC112_verifyEmailTextbox(WebElement element, WebElement submitButton, int len,
			String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		if (element != null) {
			if (isValid) {
				//vaild case
				String data1 = ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String data2 = ActWebGen.randomAlpha(len / 2) + "."
						+ ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String data3 = ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com.jp";
				String data4 = ActWebGen.randomAlpha(len / 2) + "+"
						+ ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String data5 = ActWebGen.randomNumber(len) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String data6 = ActWebGen.randomAlpha(len) + "@"
						+ ActWebGen.randomAlpha(len / 2) + "-"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String data7 = ActWebGen.randomAlpha(len / 2) + "-"
						+ ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String data8 = "___________" + "@" + ActWebGen.randomAlpha(len / 2)
						+ ".com";
				String data9 = " " + ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com ";
				String data10 = "     " + ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com    ";
				String[] arrData = new String[] { data1, data2, data3, data4,
						data5, data6, data7, data8, data9, data10 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					ActWebGen.waitVisible(submitButton, 10);
					submitButton.click();					
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage,data);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			} else {
				//invalid case
				String dataInvalid1 = ActWebGen.randomAlpha(len);
				String dataInvalid2 = ActWebGen.randomAlpha(len / 2) + "."
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid3 = ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2);
				String dataInvalid4 = ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid5 = ActWebGen.randomSpecial(len / 2) + "@"
						+ ActWebGen.randomSpecial(len / 2) + "@"
						+ ActWebGen.randomSpecial(len / 2) + ".com";
				String dataInvalid6 = "@" + ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid7 = "Joe Smith <email@domain.com> ";
				String dataInvalid8 = "." + ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid9 = ActWebGen.randomAlpha(len / 2) + "." + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid10 = ActWebGen.randomAlpha(len / 2) + ".."
						+ ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid11 = ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + "..com";
				String dataInvalid12 = "Nguyễn Hồng Gấm" + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid13 = ActWebGen.randomAlpha(len / 2) + "@"
						+ ActWebGen.randomAlpha(len / 2) + ".com (Joe Smith)";
				String dataInvalid14 = ActWebGen.randomAlpha(len / 2) + "@-"
						+ ActWebGen.randomAlpha(len / 2) + ".com";
				String dataInvalid15 = ActWebGen.randomAlpha(len / 2)
						+ "@111.222.333.44444";
				String dataInvalid16 = ActWebGen.randomAlpha(len / 2)
						+ "@[123.123.123.123]";
				String dataInvalid17 = "\""+ ActWebGen.randomAlpha(len / 2) + "\""
						+ "@domain.com";
		
				String[] arrInvalidData = new String[] { dataInvalid1,
						dataInvalid2, dataInvalid3, dataInvalid4, dataInvalid5,
						dataInvalid6, dataInvalid7, dataInvalid8, dataInvalid9,
						dataInvalid10, dataInvalid11, dataInvalid12,
						dataInvalid13, dataInvalid14, dataInvalid15, dataInvalid16,dataInvalid17};
				for (String data : arrInvalidData) {
					element.clear();
					element.sendKeys(data);
					ActWebGen.waitVisible(submitButton, 10);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextContains(errorElement, errorMessage);
					//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
					VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}

	/**
	 * CTC13	Validate Phone Number text box with valid values. 
	 * CTC14	Validate Phone Number text box with invalid values.
	 */
	public static void CTC13_CTC14_verifyPhoneNumberTextbox(By by, WebElement submitButton, int len, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC13_CTC14_verifyPhoneNumberTextbox(element,submitButton, len, errorMessage, isValid);
	}

	public static void CTC13_CTC14_verifyPhoneNumberTextbox(WebElement element, WebElement submitButton, int len,
			String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		if (element != null) {
			if (isValid) {
				String data1 = "9" + ActWebGen.randomNumber(9);
				String data2 = "1" + ActWebGen.randomNumber(10);
				String data3 = " " + ActWebGen.randomNumber(10);
				String data4 = "9" + ActWebGen.randomNumber(9) + " ";
				String data5 = "9" + ActWebGen.randomNumber(4) + " "
						+ ActWebGen.randomNumber(5);
				String[] arrData = new String[] { data1, data2, data3, data4,
						data5 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			} else {
				String dataInvalid1 = ActWebGen.randomSpecial(len);
				String dataInvalid2 = ActWebGen.randomAlpha(len);
				String dataInvalid3 = "1 = 1";
				String dataInvalid4 = "<script>alert(document.cookie);</script>";
				String dataInvalid5 = "1" + ActWebGen.randomNumber(9);
				String[] arrInvalidData = new String[] { dataInvalid1,
						dataInvalid2, dataInvalid3, dataInvalid4, dataInvalid5 };
				for (String data : arrInvalidData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();					
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextContains(errorElement, errorMessage);
					//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
					VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}

	/**
	 * CTC15	Validate Credit Card text box with valid values. 
	 * CTC16	Validate Credit Card text box with invalid values.
	 */
	public static void CTC15_CTC16_verifyCreditCardTextbox(By by, WebElement submitButton, int len, 
			String errorMessage, boolean isValid) {
		WebElement element = ActWebGen.getElement(by);
		CTC15_CTC16_verifyCreditCardTextbox(element,submitButton, len, errorMessage, isValid);
	}

	public static void CTC15_CTC16_verifyCreditCardTextbox(WebElement element, WebElement submitButton, int len,
			String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		if (element != null) {
			if (isValid) {
				String data1 = "378282246310005";
				String data2 = "378734493671000";
				String data3 = "5610591081018250";
				String data4 = "30569309025904";
				String data5 = "4242424242424242";
				String data6 = "4000056655665556";
				String data7 = "5555555555554444";
				String data8 = "5200828282828210";
				String data9 = "5105105105105100";
				String data10 = " " + "5105105105105100";
				String data11 = "5105105105105100" + " ";
				String data12 = "51051051" + " " + "05105100";
				String[] arrData = new String[] { data1, data2, data3, data4,
						data5, data6, data7, data8, data9, data10, data11,
						data12 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			} else {
				String dataInvalid1 = ActWebGen.randomSpecial(len);
				String dataInvalid2 = ActWebGen.randomAlpha(len);
				String dataInvalid3 = "1 = 1";
				String dataInvalid4 = "<script>alert(document.cookie);</script>";
				String[] arrInvalidData = new String[] { dataInvalid1,
						dataInvalid2, dataInvalid3, dataInvalid4 };
				for (String data : arrInvalidData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextContains(errorElement, errorMessage);
					//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
					VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}
	
	
	/**
	 * CTC17	Validate URL text box with valid values. 
	 * CTC18	Validate URL text box with invalid values.
	 */
	public static void CTC17_CTC18_verifyURLTextbox(WebElement element, WebElement submitButton, int len,
			 String errorMessage, boolean isValid) {
		// TODO Auto-generated method stub
		
		
		if (element != null) {
			if (isValid) {
				String data1 = "http://www.example.com";
				String data2 = "https://www.example.com";
				String data3 = "www.example.com";
				String data4 = "example.com";
				String data5 = "http://example.com";
				String data6 = "https://www.EXAMPLE.cOm";
				String data7 = "http://www.example.com/foo/bar";
				String data8 = "http://www.example.com?foo=BaR";
				String data9 = "http://www.example.com:8080";
				String data10 = "www.example.com/foo/捦挺挎/bar";
				String data11 = "1964thetribute.com";
				String data12 = "http://www.example.mil";
				String data13 = " " + "http://www.example.mil";
				String data14 = "http://www.example.mil" + "  ";
				String[] arrData = new String[] { data1, data2, data3, data4,
						data5, data6, data7, data8, data9, data10, data11,
						data12, data13, data14 };
				for (String data : arrData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextNotContain(errorElement, errorMessage);
					//VerWebGen.verifyTextNotContain_v2(errorElement, errorMessage, data);
					//VerWebGen.verifyNotDisplayed(errorElement);
					VerWebGen.verifyNotExist_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
	
				}
			} else {
				String dataInvalid1 = "http://examplecom";
				String dataInvalid2 = "http://example.";
				String dataInvalid3 = ".com";
				String dataInvalid4 = "http://example url.com";
				String dataInvalid5 = "http:\\example.com";
				String dataInvalid6 = "http://";
				String dataInvalid7 = "http://www.";
				String dataInvalid8 = "http:example.com";
				String[] arrInvalidData = new String[] { dataInvalid1, dataInvalid2, dataInvalid3, dataInvalid4, 
						dataInvalid5, dataInvalid6, dataInvalid7, dataInvalid8 };
				for (String data : arrInvalidData) {
					element.clear();
					element.sendKeys(data);
					//element.sendKeys(Keys.TAB);
					submitButton.click();
					ActWebGen.sleep(1000);
					//VerWebGen.verifyTextContains(errorElement, errorMessage);
					//VerWebGen.verifyTextContains_v2(errorElement, errorMessage, data);
					VerWebGen.verifyExists_v2(ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "')]")), data);
				}
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: "
					+ element + " does not exists.");
	}
	 
	 /**
	  * CTC26	Verify functionality of Login with valid values. 
	  */
	public static void CTC26_VerifyLoginWithValidValues(String urlLogin, String username, String password,  
			String expectedMessage) {
			// TODO Auto-generated method stub		
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		 			
	}
	
	 /**
	  * CTC27	Verify user's navigation after logging in 
	  */
	public static void CTC27_VerifyNavigationAfterLogging(String urlLogin, String username, String password,  
			String expectedMessage) {
	
		String newUrl="";
		
		
		//login with valid information
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		
		//------ 3. After logged in, user is not asked to login again on another tab of the same browser.
		//------ 6. User can navigate directly to any pages of website after logging in by copying/typing the url into the address bar
		newUrl = DriverWeb.instance.getCurrentUrl();
		ActWebGen.enter(ActWebGen.getElement(By.cssSelector("body")), Keys.CONTROL + "t", 50);
		ArrayList<String> newTab = new ArrayList<String>(DriverWeb.instance.getWindowHandles());
		DriverWeb.instance.switchTo().window(newTab.get(1));
		ActWebGen.launch(newUrl);
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		
		//--------- 4. After logged in, user is not asked to login again on another window of the same browser. 
		//_driver2.get(newUrl);
		ActWebGen.enter(ActWebGen.getElement(By.cssSelector("body")), Keys.CONTROL + "n", 50);
		//String newWindow = _driver.getWindowHandle();
		for(String winHandle : DriverWeb.instance.getWindowHandles()){
			DriverWeb.instance.switchTo().window(winHandle);
		}
		ActWebGen.launch(newUrl);
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);		
		 			
	}
	
	/**
	  * CTC28	Verify functionality of Login with invalid values 
	  */
	public static void CTC28_VerifyLoginWithInvalidValues(String urlLogin, String validUsername, String validPassword,	
			String expectedMessage) {
		ActWebGen.launch(urlLogin);
				
		//1. The error message <ID> displays when clicking Login button without entering any values into username and password.
		//2. The error message <ID> displays when clicking Login button without entering value into username.
		//3. The error message <ID> displays when clicking Login button without entering value into password.
		//4. The error message <ID> displays after user logs in with invalid username and password
		//5. The error message <ID> displays after user logs in with valid username and invalid password
				
		List<Map<String, String>> myListData = new ArrayList<Map<String, String>>();
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("", "");
		
		Map<String, String> map2 = new HashMap<String, String>();
		map2.put("", validPassword);
		
		Map<String, String> map3 = new HashMap<String, String>();
		map3.put(validUsername, "");
		
		Map<String, String> map4 = new HashMap<String, String>();
		map4.put("abc@yahoo.com","123456");
		
		Map<String, String> map5 = new HashMap<String, String>();
		map5.put(validUsername, "123456");
		
		myListData.add(map1);
		myListData.add(map2);
		myListData.add(map3);
		myListData.add(map4);
		myListData.add(map5);
		
		for(Map<String, String> dataMap:myListData){
			for(Map.Entry<String, String> entryData:dataMap.entrySet()){
				PgCommon.txtUsername().clear();
				PgCommon.txtPassword().clear();
				PgCommon.txtUsername().sendKeys(entryData.getKey());
				PgCommon.txtPassword().sendKeys(entryData.getValue());
				PgCommon.btLogin().click();
				WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
				ActWebGen.waitVisible(expectedElement, 10);
				VerWebGen.verifyExists(expectedElement);
			}
		}	
	}
	
	/**
	  * CTC30	Verify functionality of Logout. 
	  */
	public static void CTC30_VerifyFunctionalityOfLogout(String urlLogin, String username, String password,  
			String expectedMessage) {
			// TODO Auto-generated method stub
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		ActWebGen.sleep(5000);	
		
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		 			
	}
	
	/**
	  * CTC31	Verify user's navigation after logging out. 
	  */
	public static void CTC31_VerifyNavigationAfterLoggingOut(String urlLogin, String username, String password,  
			String expectedMessage) {
		String newUrl="";	
		
		//--- 1. User is logged out on other tabs after user logs out on one tab of the same browser.
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		
		//Open new tab
		newUrl = DriverWeb.instance.getCurrentUrl();
		ActWebGen.enter(ActWebGen.getElement(By.cssSelector("body")), Keys.CONTROL + "t", 50);
		ArrayList<String> newTab = new ArrayList<String>(DriverWeb.instance.getWindowHandles());
		DriverWeb.instance.switchTo().window(newTab.get(1));
		ActWebGen.launch(newUrl);			
		ActWebGen.waitVisible(PgCommon.menuAfterLogin(), 10);
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		DriverWeb.instance.close();
		
		//move back old tab and check
		DriverWeb.instance.switchTo().window(newTab.get(0));
		DriverWeb.instance.findElement(By.cssSelector("body")).sendKeys(Keys.F5);
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		
		//---2. User is logged out on other windows after user logs out on one window of the same browser.
		PgCommon.txtUsername().clear();
		PgCommon.txtPassword().clear();
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		newUrl = DriverWeb.instance.getCurrentUrl();
		
		ActWebGen.enter(ActWebGen.getElement(By.cssSelector("body")), Keys.CONTROL + "n", 50);
		for(String winHandle : DriverWeb.instance.getWindowHandles()){
			DriverWeb.instance.switchTo().window(winHandle);
		}
		ActWebGen.launch(newUrl);
		ActWebGen.waitVisible(PgCommon.menuAfterLogin(), 10);
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		DriverWeb.instance.close();

		//move back old tab and check
		DriverWeb.instance.switchTo().window(newTab.get(0));
		DriverWeb.instance.findElement(By.cssSelector("body")).sendKeys(Keys.F5);
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		
		//---3. When going back to the previous page after logging out, the Login page still displays..
		PgCommon.txtUsername().clear();
		PgCommon.txtPassword().clear();
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		ActWebGen.waitVisible(PgCommon.menuAfterLogin(), 10);
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		//move back old tab and check
		DriverWeb.instance.navigate().back();
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		 			
		//---4. Login page displays after user logs out and copying/typing a url of website into the address bar
		PgCommon.txtUsername().clear();
		PgCommon.txtPassword().clear();
		PgCommon.txtUsername().sendKeys(username);
		PgCommon.txtPassword().sendKeys(password);
		PgCommon.btLogin().click();
		newUrl = DriverWeb.instance.getCurrentUrl();
		ActWebGen.waitVisible(PgCommon.menuAfterLogin(), 10);
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		//copy back old url and check
		ActWebGen.launch(newUrl);
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
	}
	
	
	/**
	  * CTC32	Verify password reset on multi-browsers. 
	  */
	/*1. User is redirected to Login page after password is reset on another browser window of the same browser application 
	 * (Steps: login system on 2 browser windows of the same browser application 
	 * -> reset pass on one of browser window 
	 * -> refresh webpage on another browser window 
	 * -> Login page displays).
	 * */
	public static void checkOnTheSameBrowser_MultiWindow(String password1, String password2, String urlLogin, String emailAccount, String passEmail,  
			String expectedMessage){
		
		String newUrl="";
		//set current password
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(password1);
		PgCommon.txtConfirmNewPassword().sendKeys(password1);
		PgCommon.btUpdatePass().click();
		
		
		//Create link for reset password
		ActWebGen.launch(urlLogin);
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		
		//Login with current password on 2 windows of the same browser
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(password1);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		newUrl = DriverWeb.instance.getCurrentUrl();
		DriverWeb.instance.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "n");
		for(String winHandle : DriverWeb.instance.getWindowHandles()){
			DriverWeb.instance.switchTo().window(winHandle);
		}
		ActWebGen.launch(newUrl);
		
		//Reset pass in window 2 and close
		mailToUser.subject = "Reset your givn password";
		mailToUser.from = "support@givn.social";
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(password2);
		PgCommon.txtConfirmNewPassword().sendKeys(password2);
		PgCommon.btUpdatePass().click();
		DriverWeb.instance.close();
		
		//come back window 1 and check
		for(String winHandle : DriverWeb.instance.getWindowHandles()){
			DriverWeb.instance.switchTo().window(winHandle);
		}
		DriverWeb.instance.navigate().refresh();
		
		//ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyNotExist(expectedElement);		
	}
	
	public static void checkOnTheSameBrowser_MultiTab(String password1, String password2, String urlLogin, String emailAccount, String passEmail,  
			String expectedMessage){
		
		
		String newUrl="";
		//set current password
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		
		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(password1);
		PgCommon.txtConfirmNewPassword().sendKeys(password1);
		PgCommon.btUpdatePass().click();
		
		//Create link for reset password
		ActWebGen.launch(urlLogin);
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		
		//Login with current password on 2 windows of the same browser
		
		ActWebGen.launch(urlLogin);
		
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(password1);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		newUrl = DriverWeb.instance.getCurrentUrl();
		ActWebGen.enter(ActWebGen.getElement(By.cssSelector("body")), Keys.CONTROL + "t", 50);
		for(String winHandle : DriverWeb.instance.getWindowHandles()){
			DriverWeb.instance.switchTo().window(winHandle);
		}
		
		
		ActWebGen.launch(newUrl);
		
		//Open link reset pass in window 2 and close
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(password2);
		PgCommon.txtConfirmNewPassword().sendKeys(password2);
		PgCommon.btUpdatePass().click();
		DriverWeb.instance.close();
		
		//come back window 1 and check
		for(String winHandle : DriverWeb.instance.getWindowHandles()){
			DriverWeb.instance.switchTo().window(winHandle);
		}
		//_driver.navigate().refresh();
		ActWebGen.launch(newUrl);
		//ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyNotExist(expectedElement);		
	}
	
	public static void checkOnDifferentBrowser(String password1, String password2, String urlLogin, String emailAccount, String passEmail,  
			String expectedMessage){
		
		
		//set current password
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		

		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(password1);
		PgCommon.txtConfirmNewPassword().sendKeys(password1);
		PgCommon.btUpdatePass().click();		
		
		//open browser 2 and login
		WebDriver _driver2 = new ChromeDriver();
		_driver2.manage().window().maximize();
		_driver2.get(urlLogin);
		_driver2.findElement(By.xpath(".//input[@id='InputEmail']")).sendKeys(emailAccount);
		_driver2.findElement(By.xpath(".//input[@id='InputPassword']")).sendKeys(password1);
		_driver2.findElement(By.xpath("//button[@type='submit']")).click();
		
		//come back browser 1 and reset password
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(password1);
		PgCommon.btLogin().click();
		//WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
	
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
			
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(password2);
		PgCommon.txtConfirmNewPassword().sendKeys(password2);
		PgCommon.btUpdatePass().click();	
		DriverWeb.instance.close();
		
		//come back browser 1 and check
		for(String winHandle : _driver2.getWindowHandles()){
			_driver2.switchTo().window(winHandle);
		}
		_driver2.navigate().refresh();
		VerWebGen.verifyNotExist(_driver2.findElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]")));
	}

	public static void CTC32_VerifyResetOnMultiBrowser(String urlLogin, String emailAccount, String passEmail,  
			String expectedMessage) {
		
			
		String password1="123456";
		String password2="1234567";
		//1. User is redirected to Login page after password is reset on another browser window of the same browser application 
		//checkOnTheSameBrowser_MultiWindow(password1,password2,urlLogin, emailAccount, passEmail,expectedMessage);
		
		//2. User is redirected to Login page after password is reset on another window of different browser application
		checkOnDifferentBrowser(password1 , password2, urlLogin, emailAccount, passEmail,expectedMessage);
		
		
		//3. User is redirected to Login page after password is reset on another browser tab.
		//checkOnTheSameBrowser_MultiTab(password1, password2, urlLogin, emailAccount, passEmail, expectedMessage);
			
	}
	
	
	/**
	  * CTC33	Verify new password is saved after being changed once 
	  */

	public static void CTC33_VerifyNewPasswordIsSaved(String urlLogin, String emailAccount, String passEmail, 
			String expectedMessage) {
		
		//1. The forgot password page displays after user clicks on the forgot password link in Login page.
		//2. Notification message <ID> displays as mockup after user enters and submit his email address on the forgot password page.
		//3. User is redirected to Login page after finishing all actions on forgot password page.
		//4. An email is sent to the email address that user enters and submit in the forgot password page.
		//5. The reset password page displays after user clicks on the link in the reset password email.
		//6. User can login with the new password and cannot login with the old one.
		//7. User can login with the new password that is the same as the old one."
		
		String oldPassword=ActWebGen.randomAlpha(10);
		String newPassword=ActWebGen.randomAlpha(10);
		
		String[] arrayData = new String[] {oldPassword, newPassword};
		
		for(String data:arrayData){
			connectMailServerAndDelete(emailAccount, passEmail);
			forgotPassLink(urlLogin, emailAccount);
			

			Mail mailToUser = new Mail(DriverWeb.instance);		
			Folder folderUser = null;
			
			mailToUser.subject = PgCommon.subjectEmail;
			mailToUser.from = PgCommon.fromEmail;
			ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
			
			PgCommon.txtNewPassword().clear();
			PgCommon.txtConfirmNewPassword().clear();
			PgCommon.txtNewPassword().sendKeys(data);
			PgCommon.txtConfirmNewPassword().sendKeys(data);
			PgCommon.btUpdatePass().click();
		}
		
		//login with new password
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(newPassword);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		
		
		//login with old password
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().clear();
		PgCommon.txtPassword().clear();
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(oldPassword);
		PgCommon.btLogin().click();
		//ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyNotExist(expectedElement);
		
	}
	
	/**
	  * CTC34	Verify new password is saved after being changed more than one time 
	  */

	public static void CTC34_VerifyNewPasswordIsSavedMoreThanOneTime(String urlLogin, String emailAccount, String passEmail, 
			String expectedMessage) {
		
		//User can login with the latest password and cannot login with the old one, 
		//the new ones before the latest one after changing password more than one time.
		
		String password1 = ActWebGen.randomAlpha(6);
		String password2 = ActWebGen.randomAlpha(6);
		String password3 = ActWebGen.randomAlpha(6);
		String password4 = ActWebGen.randomAlpha(6);
		
		String[] arrayData1 = new String[] {password1, password2, password3, password4};
		String[] arrayData2 = new String[] {password1,password2,password3};
						
		for(String data1:arrayData1){
			connectMailServerAndDelete(emailAccount, passEmail);
			forgotPassLink(urlLogin, emailAccount);

			Mail mailToUser = new Mail(DriverWeb.instance);		
			Folder folderUser = null;
			
			mailToUser.subject = PgCommon.subjectEmail;
			mailToUser.from = PgCommon.fromEmail;
			ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
			
			PgCommon.txtNewPassword().clear();
			PgCommon.txtConfirmNewPassword().clear();
			PgCommon.txtNewPassword().sendKeys(data1);
			PgCommon.txtConfirmNewPassword().sendKeys(data1);
			PgCommon.btUpdatePass().click();
		}
		//login with latest password
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().clear();
		PgCommon.txtPassword().clear();
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(password4);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		ActWebGen.hover(PgCommon.menuAfterLogin());
		PgCommon.btLogout().click();
		
		for(String data2:arrayData2){
			//login with old password
			ActWebGen.launch(urlLogin);
			PgCommon.txtUsername().sendKeys(emailAccount);
			PgCommon.txtPassword().sendKeys(data2);
			PgCommon.btLogin().click();
			VerWebGen.verifyNotExist(expectedElement);
		}
			
	}
	
	/**
	  * CTC35	Verify the expiration of link redirects to reset password page 
	  */

	public static void CTC35_VerifyExpirationResetPasswordLink(String urlLogin, String emailAccount, String passEmail, 
		 String expectedMessage) {
		
		String newPassword= ActWebGen.randomAlpha(10);
		String resetLink = "";
		
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		
		// initial email folder

		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(newPassword);
		PgCommon.txtConfirmNewPassword().sendKeys(newPassword);
		PgCommon.btUpdatePass().click();
		
		ActWebGen.launch(resetLink);
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
		
		
	}
	
	
	/**
	  * CTC36 Verify user still can use the current password after user goes to reset password page and doesn't enter new password
	  */

	public static void CTC36_VerifyUserUseCurrentPassword(String urlLogin, String emailAccount, String passEmail, 
		 String expectedMessage) {
		
		// Set current password for test account
		String oldPassword= ActWebGen.randomAlpha(10);
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		
		// initial email folder

		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(oldPassword);
		PgCommon.txtConfirmNewPassword().sendKeys(oldPassword);
		PgCommon.btUpdatePass().click();
		
		//Send request change password without updating new password
		forgotPassLink(urlLogin, emailAccount);
		
		//login with old password
		ActWebGen.launch(urlLogin);
		PgCommon.txtUsername().sendKeys(emailAccount);
		PgCommon.txtPassword().sendKeys(oldPassword);
		PgCommon.btLogin().click();
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement)	;					
		
	}
	
	
	/**
	  * CTC37	Validate matching of password and confirm password with valid values. 
	  */
	
	public static void connectMailServerAndDelete(String username, String password){
		Mail mailToUser = new Mail(DriverWeb.instance);		
		Store storeUser;
		Folder folderUser = null;
		mailToUser.username = username;
		mailToUser.password = password;
		storeUser = mailToUser.connectMailServer();
		folderUser = mailToUser.connectMailFolder(storeUser);
		mailToUser.deleteAllMail(folderUser);
	}
	
	public static void forgotPassLink(String urlLogin, String username){
		ActWebGen.launch(urlLogin);
		PgCommon.lnkForgotPass().click();
		PgCommon.txtEmailAccount().sendKeys(username);
		PgCommon.btResetPass().click();
		ActWebGen.sleep(5000);
	}
	
	public static void CTC37_VerifyPasswordAndConfirmPasswordWithValidData(String urlLogin, String emailAccount, String passEmail, 
			String expectedMessage) {
		
		String newPassword = ActWebGen.randomAlpha(10);
		
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);
		
		// initial email folder
		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		PgCommon.txtNewPassword().clear();
		PgCommon.txtConfirmNewPassword().clear();
		PgCommon.txtNewPassword().sendKeys(newPassword);
		PgCommon.txtConfirmNewPassword().sendKeys(newPassword);
		PgCommon.btUpdatePass().click();
		
		WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
		ActWebGen.waitVisible(expectedElement, 10);
		VerWebGen.verifyExists(expectedElement);
				
	}
	
	/**
	  * CTC38	Validate matching password and confirm password with invalid values. 
	  */

	public static void CTC38_VerifyPasswordAndConfirmPasswordWithInvalidData(String urlLogin, String emailAccount, String passEmail, 
			String idErrorElement) {
		//1. Leave Password blank and fill out the Confirm Password.
		//2. Fill out Password and leave the Confirm Password blank.
		//3. Leave both Password and Confirm Password blank.
		//4. Enter confirm password and password that do not match 
		//5. Execute checklist "Validate Password text box with invalid values" on both Password and Confirm Password.
		//6. Execute checklist boundary test on both textboxes Password and Confirm Password.
		List<Map<String, String>> myListData = new ArrayList<Map<String, String>>();
		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("", ActWebGen.randomAlpha(6));
		Map<String, String> map2 = new HashMap<String, String>();
		map2.put(ActWebGen.randomAlpha(6), "");
		Map<String, String> map3 = new HashMap<String, String>();
		map3.put("", "");
		Map<String, String> map4 = new HashMap<String, String>();
		map4.put(ActWebGen.randomAlpha(6),ActWebGen.randomAlpha(6));
		
		myListData.add(map1);
		myListData.add(map2);
		myListData.add(map3);
		myListData.add(map4);
		
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);

		// initial email folder
		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		for(Map<String, String> dataMap:myListData){
			for(Map.Entry<String, String> dataEntry:dataMap.entrySet()){
				PgCommon.txtNewPassword().clear();
				PgCommon.txtConfirmNewPassword().clear();
				PgCommon.txtNewPassword().sendKeys(dataEntry.getKey());
				PgCommon.txtConfirmNewPassword().sendKeys(dataEntry.getValue());
				PgCommon.btUpdatePass().click();
				WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[@id='" + idErrorElement + "' and string-length(text()) > 0 ]"));
				
				ActWebGen.waitVisible(expectedElement, 10);
				VerWebGen.verifyExists(expectedElement);
			}
		}							
	}
	
	/**
	  * CTC39	Validate Password text box with valid values. 
	  */
	
	public static void CTC39_VerifyPasswordAndConfirmPasswordWithValidValue(String urlLogin, String emailAccount, String passEmail, 
				String expectedMessage) {
		
		//1. Numbers : (eg. 1234567890, negative number -1 if supported)
		//2. Special Characters : (eg. !@#$%^&*()-_+=~`:;"'{}[]?/.>,< )
		//3. Alphabet character includes upper case and lower case (eg. ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz)
		//4. HTML Code (eg. <script>alert(document.cookie);</script>)
		String password1=ActWebGen.randomNumber(10);
		String password2=ActWebGen.randomSpecial(10);
		String password3=ActWebGen.randomAlpha(10);
		String password4="<script>alert(document.cookie);</script>";
		
		String[] arrayData = new String[] {password1, password2, password3, password4};
		
		for(String data:arrayData){
			connectMailServerAndDelete(emailAccount, passEmail);
			forgotPassLink(urlLogin, emailAccount);

			// initial email folder
			Mail mailToUser = new Mail(DriverWeb.instance);		
			Folder folderUser = null;
			
			mailToUser.subject = PgCommon.subjectEmail;
			mailToUser.from = PgCommon.fromEmail;
			ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
			
			PgCommon.txtNewPassword().clear();
			PgCommon.txtConfirmNewPassword().clear();
			PgCommon.txtNewPassword().sendKeys(data);
			PgCommon.txtConfirmNewPassword().sendKeys(data);
			PgCommon.btUpdatePass().click();
			
			WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[contains(text(),'" + expectedMessage + "')]"));
			ActWebGen.waitVisible(expectedElement, 10);
			VerWebGen.verifyExists(expectedElement);
		}						
	}
	
	
	/**
	  * CTC40	Validate Password text box with invalid values. 
	  */

	public static void CTC40_VerifyPasswordWithInvalidValue(String urlLogin, String emailAccount, String passEmail, 
			String idErrorElement) {
		//1. Spaces:
		//1.1 Beginning of string.
		//1.2 Ending of string.
		//1.3 Between the words.
		//2. SQL Injection (eg.  ' or 1 = 1 )
		String password1=" "+ActWebGen.randomAlpha(6);
		String password2=ActWebGen.randomAlpha(6)+" ";
		String password3=ActWebGen.randomAlpha(6)+" "+ ActWebGen.randomAlpha(6);
		String password4=" ' or 1 = 1";
		
		String[] arrayData = new String[]{password1, password2, password3, password4};
		
		connectMailServerAndDelete(emailAccount, passEmail);
		forgotPassLink(urlLogin, emailAccount);

		// initial email folder
		Mail mailToUser = new Mail(DriverWeb.instance);		
		Folder folderUser = null;
		
		mailToUser.subject = PgCommon.subjectEmail;
		mailToUser.from = PgCommon.fromEmail;
		ActWebGen.launch(mailToUser.getTextLinkInEmail(folderUser, "HERE"));
		
		for(String data:arrayData){
			PgCommon.txtNewPassword().clear();
			PgCommon.txtConfirmNewPassword().clear();
			PgCommon.txtNewPassword().sendKeys(data);
			PgCommon.txtConfirmNewPassword().sendKeys(data);
			PgCommon.btUpdatePass().click();
			WebElement expectedElement = ActWebGen.getElement(By.xpath(".//*[@id='" + idErrorElement + "' and string-length(text()) > 0 ]"));
			ActWebGen.waitVisible(expectedElement, 10);
			VerWebGen.verifyExists(expectedElement);
		}
	}
}