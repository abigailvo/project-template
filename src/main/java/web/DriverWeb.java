package web;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import web.function.ActWebGen;


public class DriverWeb
{
	public static WebDriver instance;
	public static AppiumDriver appiumInstance;
	public static String browser;

    public static void close()
    {
        instance.close();
    }
    
    /*
    public static void BrowserInit()
    {
        //String browser = Selenium.browser;
    	// Will be get from the configured file
    	String browser = "Chrome";

        switch (browser)
        {
            case "firefox":
                FirefoxProfile profile = new FirefoxProfile();
                String path = "D:\\Test\\";
                // Clear the cache of the browsers
                profile.setPreference("browser.download.folderList", 2);
                profile.setPreference("browser.download.dir", path);
                profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/msword, application/csv, application/ris, text/csv, image/png, application/pdf, text/html, text/plain, application/zip, application/x-zip, application/x-zip-compressed, application/download, application/octet-stream");
                //profile.SetPreference("browser.download.manager.showWhenStarting", false);
                profile.setPreference("browser.download.manager.focusWhenStarting", false);
                profile.setPreference("browser.download.useDownloadDir", true);
                profile.setPreference("browser.helperApps.alwaysAsk.force", false);
                profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
                profile.setPreference("browser.download.manager.closeWhenDone", true);
                profile.setPreference("browser.download.manager.showAlertOnComplete", false);
                profile.setPreference("browser.download.manager.useWindow", false);
                profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
                profile.setPreference("pdfjs.disabled", true);

                instance = new FirefoxDriver(profile);

                break;
            case "chrome":
                //instance = new ChromeDriver("FilePath");
            	instance = new ChromeDriver();
                break;
            case "iexplorer":
                instance = new InternetExplorerDriver();
                break;
        }

        DeleteCookies(); 

    }*/
    
    public static void deleteCookies()
    {
        if (instance == null) return;
        instance.manage().deleteAllCookies();
    }
    
    public static void switchToWindow(WebDriver driver, String name) {
		ActWebGen.sleep(5000);
		WebDriver popup = null;
		Iterator<String> windowIterator = driver.getWindowHandles().iterator();
		while (windowIterator.hasNext()) {
			String windowHandle = windowIterator.next();
			// System.out.println(windowHandle);
			popup = driver.switchTo().window(windowHandle);
			if (popup.getTitle().contains(name)) {
				break;
			}
		}
	}

	public static void deleteCookie(WebDriver driver, String domain) {
		String currentURL = driver.getCurrentUrl();
		if (!currentURL.contains(domain)) {
			driver.get("http://" + domain);
		}
		driver.manage().deleteAllCookies();
		driver.get(currentURL);
		ActWebGen.sleep(5000);
	}

	/**
	 * This method is used to automate on Web version on local machine
	 * 
	 * @param browser
	 *          : Browser to execute test on. Will be retrieved from TestNG
	 * @return WebDriver
	 * @throws MalformedURLException
	 */
	public static void initBrowser(String browser) throws MalformedURLException {
		WebDriver driver = null;
		if ("firefox".equalsIgnoreCase(browser)) {
			FirefoxProfile p = new FirefoxProfile();
			p.setEnableNativeEvents(true);
			driver = new FirefoxDriver(p);
		} else if ("safari".equalsIgnoreCase(browser)) {
			SafariOptions ops = new SafariOptions();
			ops.setUseCleanSession(true);
			driver = new SafariDriver(ops);
		} else if ("ie".equalsIgnoreCase(browser)) {

			DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
			cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
			    true);
			File file = new File("C:/selenium-tools/IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			
			driver = new InternetExplorerDriver(cap);
		} else if ("chrome".equalsIgnoreCase(browser)) {
			System.setProperty("webdriver.chrome.driver", "C:/selenium-tools/chromedriver.exe");
			
			//Added By Hung Le to handle Facebook Notification Permission
			ChromeOptions options = new ChromeOptions(); 
			Map<String, Integer> prefs = new HashMap<String, Integer>();  
			prefs.put("profile.managed_default_content_settings.notifications", 1);  
			options.setExperimentalOption("prefs", prefs);
			
			driver = new ChromeDriver(options);
		}
		DriverWeb.browser = browser;
		DriverWeb.instance = driver;
	}

	/**
	 * This method is used to automate on Web version on slave machine
	 * 
	 * @param node
	 *          : address of slave machine (ex: http://<ip address>:<port>/wd/hub)
	 * @param browser
	 *          : Browser to execute test on. Will be retrieved from TestNG
	 * @return WebDriver
	 * @throws MalformedURLException
	 */
	public static void initBrowser(String node, String browser) throws MalformedURLException {
		WebDriver driver = null;
		if (browser.equalsIgnoreCase("firefox")) {
			System.out.println(" Executing on FireFox");
			DesiredCapabilities cap = DesiredCapabilities.firefox();
			cap.setBrowserName("firefox");
			driver = new RemoteWebDriver(new URL(node), cap);
			// Puts a Implicit wait, Will wait for 10 seconds before throwing
			// exception
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.out.println(" Executing on CHROME");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setBrowserName("chrome");
			
			
			ChromeOptions options = new ChromeOptions(); 
			Map<String, Integer> prefs = new HashMap<String, Integer>();  
			prefs.put("profile.default_content_settings.cookies", 1);  
			options.setExperimentalOption("prefs", prefs);
			cap.setCapability(ChromeOptions.CAPABILITY,options );
			
			driver = new RemoteWebDriver(new URL(node), cap);
		} else if (browser.equalsIgnoreCase("ie")) {
			System.out.println(" Executing on IE");
			DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
			cap.setBrowserName("ie");
			cap.setCapability("requireWindowFocus", true);
			driver = new RemoteWebDriver(new URL(node), cap);
		} else {
			throw new IllegalArgumentException("The Browser Type is Undefined");
		}
		DriverWeb.browser = browser;
		DriverWeb.instance = driver;
	}

	/**
	 * This method is used to automate on mobile version
	 * 
	 * @param platform
	 *          : Android/iOS
	 * @param device
	 *          : device name
	 * @return WebDriver
	 * @throws MalformedURLException
	 */
	public static void getMobileDriver(String platform, String udid) {
		AppiumDriver driver = null;
		DesiredCapabilities cap = null;
		if (platform.equalsIgnoreCase("Android")) {
			File appDir = new File("C://selenium-tools//");
			File app = new File(appDir, "com.posiba.givn.apk");
			cap = new DesiredCapabilities();
			cap.setCapability("platformName", platform);
			cap.setCapability("deviceName", udid);
			cap.setCapability("app", app.getAbsolutePath());
			cap.setCapability("autoLaunch", false);

			// Get platform version
			String version = ActWebGen.getPlatformVersion(udid);
			if (version != null) {
				cap.setCapability("platformVersion", version);
			}

			try {
				driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		} else if (platform.equalsIgnoreCase("iOS")) {
			// iOS Block
		} else {
			throw new IllegalArgumentException("The OS Type is Undefined");
		}

		DriverWeb.appiumInstance = driver;
	}
	

	public static void setElementWait(int seconds) {
		// Wait for element
		DriverWeb.instance.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}

	public static void setPageWait(int seconds) {
		// Wait for page load
		DriverWeb.instance.manage().timeouts().pageLoadTimeout(seconds, TimeUnit.SECONDS);
	}
}

